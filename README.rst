Tsg Prueba
======

TSG Requiere la administración de sus recursos físicos tangibles (bienes muebles e inmuebles) para llevar el control de estos y poder asignarlos de forma interna a una persona o área encargada de usar y responsabilizarse de él, es necesario conocer de cada recurso su serial, marca, tipo (Tecnológico, Oficina, Cafetería), proveedor, valor comercial, fecha de compra y estado (Buen Estado, Mal estado, Nuevo). Del responsable es necesario saber nombre del responsable directo, teléfono, tipo (Área o Persona) y fecha de asignación del recurso, es importante saber que un responsable puede tener más de un recurso asignado.

Requisitos:
-------

- Python 3.6
- pip


Instalacion
-------

Clonar el repositorio::
    
    git clone https://gitlab.com/martinezvictor84/apitsg.git
    cd apitsg
    


Crear virtualenv y activarlo ::

    python3 -m venv venv
    . venv/bin/activate

En Windows cmd::

    py -3 -m venv venv
    venv\Scripts\activate.bat

Instalar Dependencias::

    pip install -e .


Ejecutar
-------
En linux::

    export FLASK_APP=app.py
    export FLASK_ENV=development
    flask run

En Windows::

    set FLASK_APP=app.py
    set FLASK_ENV=development
    flask run

Base datos
------

SQLite::

    #ejeucatar para generar esquema
    flask init-db
    
MySQL::

    ejecutar el script db/schemaMySQL.sql en la base de datos

abrir http://127.0.0.1:5000 en el navegador
