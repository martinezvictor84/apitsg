import io

from setuptools import find_packages, setup

with io.open('README.rst', 'rt', encoding='utf8') as f:
    readme = f.read()

setup(
    name='apitgs',
    version='1.0.0',
    url='',
    maintainer='Victor Martinez',
    maintainer_email='martinezvictor84@gmail.com',
    description='prueba de ingreso para TGS',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask',
        'flask_sqlalchemy',
        'pymysql',
        'mysqlclient',
        'flask_cors',
        'pysqlite3 ',
    ],
)
