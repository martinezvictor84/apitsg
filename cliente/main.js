var api = {
  //urlBase:"http://localhost:5000"
  urlBase:"http://martinezvictor84.pythonanywhere.com"
};

Vue.component('table-personas', {
  props:['title'],
  data : function(){
    return {
      headers:['Nombre','Tipo Responsable','Telefono','Acciones'],
      userEdit:false,
      users:[],
      tipos:[],
      formData:
        {
          nombre:"",
          persona_tipo_id :"",
          telefono:""
        }
    }
  },
  mounted:function(){
     axios
    .get(api.urlBase+'/persona/tipo')
    .then(response => (this.tipos = response.data) );
     axios
    .get(api.urlBase+'/persona')
    .then(response => (this.users = response.data ));
  },
  methods:{
    save : function(){
      if(this.userEdit){
        axios
        .put(api.urlBase+'/persona/'+this.formData.id,this.formData)
        .then((response) => {
          var index = this.getIndexById(this.users,this.formData.id);
          this.users[index].nombre = this.formData.nombre;
          this.users[index].persona_tipo_id = this.formData.persona_tipo_id;
          this.users[index].telefono = this.formData.telefono;
          $('#employeeModal').modal('hide');
        });
      }
      else{
        axios
        .post(api.urlBase+'/persona',this.formData)
        .then((response) => {
          this.users.push({
            id:response.data.id,
            nombre:this.formData.nombre,
            persona_tipo_id:this.formData.persona_tipo_id,
            telefono:this.formData.telefono
          });
          $('#employeeModal').modal('hide');
        });
      }
    },
    edit : function(user){
      this.formData.id = user.id;
      this.formData.nombre = user.nombre;
      this.formData.persona_tipo_id = user.persona_tipo_id;
      this.formData.telefono = user.telefono;
      this.userEdit=true;
    },
    newU : function(){
      this.userEdit=false;
      this.formData={};
    },
    deleteU :function(){
      axios
      .delete(api.urlBase+'/persona/'+this.formData.id)
      .then((response) => {
        var index = this.getIndexById(this.users,this.formData.id);
        this.users.splice(index,1);
        $('#deleteEmployeeModal').modal('hide');
      });
    },
    deleteConfirmacion: function(user){
      this.formData.id=user.id;
    },
    findById:function(items,id){
        var index = items.findIndex(function(item,i){
          return item.id === id;
        });
        return index>-1 ? items[index].nombre:'';
    },
    getIndexById:function(items,id){
        var index = items.findIndex(function(item,i){
          return item.id === id;
        });
        return index;
    }
  },
  template: `<div class="table-wrapper">
               <div class="table-title">
                 <div class="row">
                   <div class="col-sm-6">
                     <h2>{{title}}</b></h2>
                   </div>
                   <div class="col-sm-6">
                     <a href="#employeeModal" @click="newU()" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Agregar Responsable</span></a>
                   </div>
                 </div>
               </div>
               <table class="table table-striped table-hover">
                 <thead>
                   <tr>
                     <th v-for='header in headers'>{{header}}</th>
                   </tr>
                 </thead>
                 <tbody>
                   <tr v-for='user in users'>
                     <td>{{user.nombre}}</td>
                     <td>{{findById(tipos,user.persona_tipo_id)}}</td>
                     <td>{{user.telefono}}</td>
                     <td>
                       <a href="#employeeModal" @click="edit(user)" class="edit" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                       <a href="#deleteEmployeeModal" @click="deleteConfirmacion(user)"class="delete" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                     </td>
                   </tr>
                 </tbody>
               </table>
               <!-- Edit Modal HTML -->
               <div id="employeeModal" class="modal fade">
                 <div class="modal-dialog modal-sm">
                   <div class="modal-content">
                     <form id="savePersonaForm" @submit.prevent="save()">
                       <div class="modal-header">
                         <h4 class="modal-title">{{ userEdit? "Editar " : "Crear " }} Responsable</h4>
                         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                       </div>
                       <div class="modal-body">
                         <div class="form-group">
                           <label>Nombre</label>
                           <input type="text" class="form-control" v-model="formData.nombre" required>
                           <input type="hidden" v-model="formData.id" required>
                         </div>
                         <div class="form-group">
                           <label>Tipo:</label>
                           <select name="tipo_persona_id" class="form-control" required v-model=formData.persona_tipo_id>
                              <option value="">Seleccio un Tipo</option>
                              <option v-for='tipo in tipos' :value=tipo.id>{{tipo.nombre}}</option>
                           </select>
                         </div>
                         <div class="form-group">
                           <label>Telefono</label>
                           <input type="text" class="form-control" v-model="formData.telefono" required>
                         </div>
                       </div>
                       <div class="modal-footer">
                         <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                         <input type="submit" class="btn btn-info" value="Save">
                       </div>
                     </form>
                   </div>
                 </div>
               </div>
               <!-- Delete Modal HTML -->
               <div id="deleteEmployeeModal" class="modal fade">
                 <div class="modal-dialog">
                   <div class="modal-content">
                     <form @submit.prevent="deleteU()">
                       <div class="modal-header">
                         <h4 class="modal-title">Eliminar persona</h4>
                         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                       </div>
                       <div class="modal-body">
                         <p>Esta seguro de eliminar este recurso?</p>
                         <p class="text-warning"><small>Esta accion no se puede recuperar.</small></p>
                       </div>
                       <div class="modal-footer">
                         <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                         <input type="submit" class="btn btn-danger" value="Delete">
                       </div>
                     </form>
                     </div>
                  </div>
                </div>
             </div>`
})

Vue.component('table-recursos', {
  props:['title'],
  data : function(){
    return {
      headers:['Serial','Nombre','Estado','Tipo','Marca','Acciones'],
      modalTitle:'Ver Recurso',
      modalOnlyView:true,
      editM:true,
      users:{},
      recursos:{},
      tipos:[],
      proveedores:[],
      responsables:{},
      estados:[],
      formData:{}
    }
  },
  mounted:function(){
     axios
    .get(api.urlBase+'/recurso/estado')
    .then(response => (this.estados = response.data) );

     axios
    .get(api.urlBase+'/recurso/proveedor')
    .then(response => (this.proveedores = response.data ));

     axios
    .get(api.urlBase+'/recurso/tipo')
    .then(response => (this.tipos = response.data ));

     axios
    .get(api.urlBase+'/persona')
    .then(response => (this.responsables = response.data ));

     axios
    .get(api.urlBase+'/recurso')
    .then(response => (this.recursos = response.data ));
  },
  methods:{
    save : function(){
      if(this.editM){
        axios
        .put(api.urlBase+'/recurso/'+this.formData.id,this.formData)
        .then((response) => {
          var index = this.getIndexById(this.recursos,this.formData.id);
          this.recursos[index].serial = this.formData.serial;
          this.recursos[index].nombre = this.formData.nombre;
          this.recursos[index].recurso_tipo_id = this.formData.recurso_tipo_id;
          this.recursos[index].recurso_estado_id = this.formData.recurso_estado_id;
          this.recursos[index].persona_id = this.formData.persona_id;
          this.recursos[index].proveedor_id = this.formData.proveedor_id;
          this.recursos[index].marca = this.formData.marca;
          $('#recursoModal').modal('hide');
        });
      }
      else{
        axios
        .post(api.urlBase+'/recurso',this.formData)
        .then((response) => {
          this.recursos.push({
            id:response.data.id,
            serial : this.formData.serial,
            nombre : this.formData.nombre,
            recurso_tipo_id : this.formData.recurso_tipo_id,
            recurso_estado_id : this.formData.recurso_estado_id,
            persona_id : this.formData.persona_id,
            proveedor_id : this.formData.proveedor_id,
            marca : this.formData.marca
          });
          $('#recursoModal').modal('hide');
        });
      }
    },
    edit : function(recurso){
      this.formData ={};
      this.formData.id = recurso.id;
      this.formData.nombre = recurso.nombre;
      this.formData.serial = recurso.serial;
      this.formData.recurso_tipo_id = recurso.recurso_tipo_id;
      this.formData.recurso_estado_id = recurso.recurso_estado_id;
      this.formData.persona_id = recurso.persona_id;
      this.formData.proveedor_id = recurso.proveedor_id;
      this.formData.marca = recurso.marca;
      this.formData.creado_en = recurso.creado_en;
      this.formData.asignado_en = recurso.asignado_en;

      this.editM=true;
      this.modalOnlyView=true;
    },
    newU : function(){
      this.editM=false;
      this.modalOnlyView=false;
      this.formData={};
    },
    deleteU :function(){
      axios
      .delete(api.urlBase+'/recurso/'+this.formData.id)
      .then((response) => {
        var index = this.getIndexById(this.recursos,this.formData.id);
        this.recursos.splice(index,1);
        $('#deleteRecursoModal').modal('hide');
      });
    },
    deleteConfirmacion: function(recurso){
      this.formData.id=recurso.id;
    },
    findById:function(items,id){
        var index = items.findIndex(function(item,i){
          return item.id === id;
        });
        return index>-1 ? items[index].nombre:'';
    },
    getIndexById:function(items,id){
        var index = items.findIndex(function(item,i){
          return item.id === id;
        });
        return index;
    },
    filter:function(){
      data=$('#filtro').serialize();
      axios
     .get(api.urlBase+'/recurso?'+data)
     .then(response => (this.recursos = response.data ));
    }
  },
  template: `<div class="table-wrapper">
               <div class="table-title">
                 <div class="row">
                   <div class="col-sm-6">
                     <h2>{{title}}</b></h2>
                   </div>
                   <div class="col-sm-6">
                     <a href="#recursoModal" @click="newU()" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Agregar Recurso</span></a>
                   </div>
                 </div>
               </div>
               <form id="filtro"></form>
               <table class="table table-striped table-hover">
                 <thead>
                   <tr>
                     <th v-for='header in headers'>{{header}}</th>
                   </tr>
                 </thead>
                 <tbody>
                   <tr v-for='recurso in recursos'>
                     <td>{{recurso.serial}}</td>
                     <td>{{recurso.nombre}}</td>
                     <td>{{findById(estados,recurso.recurso_estado_id)}}</td>
                     <td>{{findById(tipos,recurso.recurso_tipo_id)}}</td>
                     <td>{{recurso.marca}}</td>
                     <td>
                       <a href="#recursoModal" @click="edit(recurso)" class="edit" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                       <a href="#deleteRecursoModal" @click="deleteConfirmacion(recurso)" class="delete" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
                     </td>
                   </tr>
                   <tr>
                     <td >
                      <input @keyup=filter type="text" name="serial" class="form-control" placeholder="Serial.."  form="filtro" />
                     </td>
                     <td colspan=2>
                      <input @keyup=filter type="text" name="responsable" class="form-control" placeholder="Responsale.."  form="filtro" />
                     </td>
                     <td>
                       <select @change=filter name="tipo" class="form-control" form="filtro">
                        <option value="" selected>Seleccione un Tipo</option>
                        <option v-for='tipo in tipos' :value=tipo.id>{{tipo.nombre}}</option>
                       </select>
                     </td>
                     <td>
                       <input @keyup=filter type="text" name="marca" class="form-control" placeholder="Marca.." form="filtro" />
                     </td>
                     <td></td>
                   </tr>
                 </tbody>
               </table>
               <!-- Edit Modal HTML -->
               <div id="recursoModal" class="modal fade">
                 <div class="modal-dialog modal-lg">
                   <div class="modal-content">
                     <form id="savePersonaForm" @submit.prevent="save()">
                       <div class="modal-header">
                         <h4 class="modal-title">{{ modalTitle }}</h4>
                         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                       </div>
                       <div class="modal-body">
                         <div class="row">
                          <div class="col-md-5">
                            <div class="form-group">
                              <label>Serial:</label>
                              <input type="text" class="form-control" v-model="formData.serial" required>
                              <input type="hidden" v-model="formData.id" required>
                            </div>
                          </div>
                          <div class="col-md-7">
                            <div class="form-group">
                              <label>Nombre:</label>
                              <input type="hidden" v-model="formData.id" required>
                              <input type="text" class="form-control" v-model="formData.nombre" required>
                            </div>
                          </div>
                         </div>
                         <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Tipo:</label>
                                <select class="form-control" required v-model=formData.recurso_tipo_id>
                                   <option value="" selected>Seleccione un Tipo</option>
                                   <option v-for='tipo in tipos' :value=tipo.id>{{tipo.nombre}}</option>
                                </select>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                              <label>Estado:</label>
                              <select class="form-control" required v-model=formData.recurso_estado_id>
                              <option value="" selected>Seleccione un Estado</option>
                              <option v-for='estado in estados' :value=estado.id>{{estado.nombre}}</option>
                              </select>
                              </div>
                            </div>
                         </div>
                         <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Proveedor:</label>
                                <select class="form-control" required v-model=formData.proveedor_id>
                                  <option value="">Seleccione un Proveedor</option>
                                  <option v-for='proveedor in proveedores' :value=proveedor.id>{{proveedor.nombre}}</option>
                                </select>
                              </div>

                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label>Marca:</label>
                                <input type="text" class="form-control" v-model="formData.marca" required>
                              </div>
                            </div>
                         </div>
                         <div class="row">
                           <div class="col-md-12">
                             <div class="form-group">
                               <label>Responsable:</label>
                               <select class="form-control" v-model=formData.persona_id>
                                 <option value="">Seleccione un Responsable</option>
                                 <option v-for='responsable in responsables' :value=responsable.id>{{responsable.nombre}}</option>
                               </select>
                             </div>
                           </div>
                         </div>
                         <div v-show="modalOnlyView" class="form-group">
                           <label>Creado en: {{formData.creado_en}}</label><br>
                           <label>Ultima Asignacion en: {{formData.asignado_en}}</label>
                        </div>
                       </div>
                       <div class="modal-footer">
                         <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                         <input type="submit" class="btn btn-info" value="Save">
                       </div>
                     </form>
                   </div>
                 </div>
               </div>
               <!-- Delete Modal HTML -->
               <div id="deleteRecursoModal" class="modal fade">
                 <div class="modal-dialog">
                   <div class="modal-content">
                     <form @submit.prevent="deleteU()">
                       <div class="modal-header">
                         <h4 class="modal-title">Eliminar Recurso</h4>
                         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                       </div>
                       <div class="modal-body">
                         <p>Esta seguro de eliminar este recurso?</p>
                         <p class="text-warning"><small>Esta accion no se puede recuperar.</small></p>
                       </div>
                       <div class="modal-footer">
                         <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                         <input type="submit" class="btn btn-danger" value="Delete">
                       </div>
                     </form>
                     </div>
                  </div>
                </div>
               </div>`
})

var app = new Vue({
  el:"#main",
  data:{
    title:"Crud Recursos"
  }
});
