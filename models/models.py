# coding: utf-8
from sqlalchemy import Column, DateTime, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.schema import FetchedValue
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
class Persona(db.Model):
    __tablename__ = 'persona'

    id = db.Column(db.Integer, primary_key=True)
    persona_tipo_id = db.Column(db.ForeignKey('persona_tipo.id'), nullable=False, index=True)
    nombre = db.Column(db.String(100))
    telefono = db.Column(db.String(20))
    enabled = db.Column(db.Boolean,server_default=db.FetchedValue())
    persona_tipo = db.relationship('PersonaTipo', primaryjoin='Persona.persona_tipo_id == PersonaTipo.id', backref='persona')
    def serialize(self):
        data ={
            'id':self.id,
            'nombre':self.nombre,
            'telefono':self.telefono,
            'persona_tipo_id': self.persona_tipo_id if self.persona_tipo_id else None,
        }
        return data

class PersonaTipo(db.Model):
    __tablename__ = 'persona_tipo'

    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(128))
    def serialize(self):
        data ={
            'id':self.id,
            'nombre':self.nombre,
        }
        return data

class Proveedor(db.Model):
    __tablename__ = 'proveedor'

    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(100))
    def __str__(self):
        return  self.id
    def serialize(self):
        data={
            'id':self.id,
            'nombre':self.nombre,
        }
        return data

class Recurso(db.Model):
    __tablename__ = 'recurso'

    id = db.Column(db.Integer, primary_key=True, nullable=False)
    recurso_estado_id = db.Column(db.ForeignKey('recurso_estado.id'), nullable=False, index=True)
    recurso_tipo_id = db.Column(db.ForeignKey('recurso_tipo.id'), index=True)
    proveedor_id = db.Column(db.ForeignKey('proveedor.id'),  index=True)
    persona_id = db.Column(db.ForeignKey('persona.id'), index=True)
    serial = db.Column(db.String(100), nullable=False, unique=True)
    marca = db.Column(db.String(100))
    creado_en = db.Column(db.DateTime, server_default=db.FetchedValue())
    asignado_en = db.Column(db.DateTime)
    nombre = db.Column(db.String(100))

    persona = db.relationship('Persona', primaryjoin='Recurso.persona_id == Persona.id', backref='recursoes')
    proveedor = db.relationship('Proveedor', primaryjoin='Recurso.proveedor_id == Proveedor.id', backref='recursoes')
    recurso_estado = db.relationship('RecursoEstado', primaryjoin='Recurso.recurso_estado_id == RecursoEstado.id', backref='recursoes')
    recurso_tipo = db.relationship('RecursoTipo', primaryjoin='Recurso.recurso_tipo_id == RecursoTipo.id', backref='recursoes')
    def serialize(self):
        data = {
            'id':self.id,
            'serial':self.serial,
            'nombre':self.nombre,
            'marca':self.marca,
            'creado_en':self.creado_en,
            'asignado_en':self.asignado_en,
            'persona_id': self.persona_id if self.persona else None,
            'proveedor_id':self.proveedor_id if self.proveedor else None,
            'recurso_estado_id':self.recurso_estado_id if self.recurso_estado else None,
            'recurso_tipo_id':self.recurso_tipo_id if self.recurso_tipo else None,
        }
        return data

class RecursoEstado(db.Model):
    __tablename__ = 'recurso_estado'

    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(45))
    def serialize(self):
        data ={
            'id':self.id,
            'nombre':self.nombre,
        }
        return data

class RecursoTipo(db.Model):
    __tablename__ = 'recurso_tipo'

    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(100))
    def serialize(self):
        data ={
            'id':self.id,
            'nombre':self.nombre,
        }
        return data
