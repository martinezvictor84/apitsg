-- -----------------------------------------------------
-- Table persona_tipo
-- -----------------------------------------------------
DROP TABLE IF EXISTS persona_tipo ;

CREATE TABLE IF NOT EXISTS persona_tipo (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nombre VARCHAR(128) NULL
  );


-- -----------------------------------------------------
-- Table persona
-- -----------------------------------------------------
DROP TABLE IF EXISTS persona ;

CREATE TABLE IF NOT EXISTS persona (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  persona_tipo_id INT NOT NULL,
  nombre VARCHAR(100) NULL,
  telefono VARCHAR(20) NULL,
  enabled TINYINT(1) NULL DEFAULT 1,
  -- INDEX fk_persona_persona_tipo1_idx (persona_tipo_id ASC),
  CONSTRAINT fk_persona_persona_tipo1
    FOREIGN KEY (persona_tipo_id)
    REFERENCES persona_tipo (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
  );


-- -----------------------------------------------------
-- Table recurso_estado
-- -----------------------------------------------------
DROP TABLE IF EXISTS recurso_estado ;

CREATE TABLE IF NOT EXISTS recurso_estado (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nombre VARCHAR(45) NULL
  )
;


-- -----------------------------------------------------
-- Table recurso_tipo
-- -----------------------------------------------------
DROP TABLE IF EXISTS recurso_tipo ;

CREATE TABLE IF NOT EXISTS recurso_tipo (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nombre VARCHAR(100) NULL
  );


-- -----------------------------------------------------
-- Table proveedor
-- -----------------------------------------------------
DROP TABLE IF EXISTS proveedor ;

CREATE TABLE IF NOT EXISTS proveedor (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nombre VARCHAR(100) NULL
  );


-- -----------------------------------------------------
-- Table recurso
-- -----------------------------------------------------
DROP TABLE IF EXISTS recurso ;

CREATE TABLE IF NOT EXISTS recurso (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  recurso_estado_id INT NULL,
  recurso_tipo_id INT NULL,
  proveedor_id INT NULL,
  persona_id INT NULL,
  serial VARCHAR(100) NOT NULL,
  marca VARCHAR(100) NULL,
  creado_en DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  asignado_en DATETIME NULL,
  nombre VARCHAR(100) NULL,
  -- INDEX fk_recurso_recurso_estado_idx (recurso_estado_id ASC),
  -- INDEX fk_recurso_recurso_tipo1_idx (recurso_tipo_id ASC),
  -- INDEX fk_recurso_proveedor1_idx (proveedor_id ASC),
  -- INDEX fk_recurso_persona1_idx (persona_id ASC),
  -- UNIQUE INDEX serial_UNIQUE (serial ASC),
  CONSTRAINT fk_recurso_recurso_estado
    FOREIGN KEY (recurso_estado_id)
    REFERENCES recurso_estado (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_recurso_recurso_tipo1
    FOREIGN KEY (recurso_tipo_id)
    REFERENCES recurso_tipo (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_recurso_proveedor1
    FOREIGN KEY (proveedor_id)
    REFERENCES proveedor (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_recurso_persona1
    FOREIGN KEY (persona_id)
    REFERENCES persona (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
  );

-- -----------------------------------------------------
-- Data for table persona_tipo
-- -----------------------------------------------------


INSERT INTO persona_tipo (id, nombre) VALUES (1, 'Area');
INSERT INTO persona_tipo (id, nombre) VALUES (2, 'Persona');




-- -----------------------------------------------------
-- Data for table recurso_estado
-- -----------------------------------------------------


INSERT INTO recurso_estado (id, nombre) VALUES (1, 'Mal Estado');
INSERT INTO recurso_estado (id, nombre) VALUES (2, 'Buen Estado');
INSERT INTO recurso_estado (id, nombre) VALUES (3, 'Nuevo');



-- -----------------------------------------------------
-- Data for table recurso_tipo
-- -----------------------------------------------------


INSERT INTO recurso_tipo (id, nombre) VALUES (1, 'Tecnológico');
INSERT INTO recurso_tipo (id, nombre) VALUES (2, 'Oficina');
INSERT INTO recurso_tipo (id, nombre) VALUES (3, 'Cafetería');



-- -----------------------------------------------------
-- Data for table proveedor
-- -----------------------------------------------------


INSERT INTO proveedor (id, nombre) VALUES (1, 'TIGOUNE');
INSERT INTO proveedor (id, nombre) VALUES (2, 'CLARO');
INSERT INTO proveedor (id, nombre) VALUES (3, 'IMUSA');
INSERT INTO proveedor (id, nombre) VALUES (4, 'LENOVO');
