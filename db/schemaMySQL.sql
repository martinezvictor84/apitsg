-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema tgs_prueba
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `tgs_prueba` ;

-- -----------------------------------------------------
-- Schema tgs_prueba
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tgs_prueba` DEFAULT CHARACTER SET utf8 ;
USE `tgs_prueba` ;

-- -----------------------------------------------------
-- Table `tgs_prueba`.`persona_tipo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tgs_prueba`.`persona_tipo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(128) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tgs_prueba`.`persona`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tgs_prueba`.`persona` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `persona_tipo_id` INT NOT NULL,
  `nombre` VARCHAR(100) NULL,
  `telefono` VARCHAR(20) NULL,
  `enabled` TINYINT(1) NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  INDEX `fk_persona_persona_tipo1_idx` (`persona_tipo_id` ASC),
  CONSTRAINT `fk_persona_persona_tipo1`
    FOREIGN KEY (`persona_tipo_id`)
    REFERENCES `tgs_prueba`.`persona_tipo` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tgs_prueba`.`recurso_estado`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tgs_prueba`.`recurso_estado` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tgs_prueba`.`recurso_tipo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tgs_prueba`.`recurso_tipo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tgs_prueba`.`proveedor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tgs_prueba`.`proveedor` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `tgs_prueba`.`recurso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tgs_prueba`.`recurso` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `recurso_estado_id` INT NULL,
  `recurso_tipo_id` INT NULL,
  `proveedor_id` INT NULL,
  `persona_id` INT NULL,
  `serial` VARCHAR(100) NOT NULL,
  `marca` VARCHAR(100) NULL,
  `creado_en` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `asignado_en` DATETIME NULL,
  `nombre` VARCHAR(100) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_recurso_recurso_estado_idx` (`recurso_estado_id` ASC),
  INDEX `fk_recurso_recurso_tipo1_idx` (`recurso_tipo_id` ASC),
  INDEX `fk_recurso_proveedor1_idx` (`proveedor_id` ASC),
  INDEX `fk_recurso_persona1_idx` (`persona_id` ASC),
  UNIQUE INDEX `serial_UNIQUE` (`serial` ASC),
  CONSTRAINT `fk_recurso_recurso_estado`
    FOREIGN KEY (`recurso_estado_id`)
    REFERENCES `tgs_prueba`.`recurso_estado` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_recurso_recurso_tipo1`
    FOREIGN KEY (`recurso_tipo_id`)
    REFERENCES `tgs_prueba`.`recurso_tipo` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_recurso_proveedor1`
    FOREIGN KEY (`proveedor_id`)
    REFERENCES `tgs_prueba`.`proveedor` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_recurso_persona1`
    FOREIGN KEY (`persona_id`)
    REFERENCES `tgs_prueba`.`persona` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `tgs_prueba`.`persona_tipo`
-- -----------------------------------------------------
START TRANSACTION;
USE `tgs_prueba`;
INSERT INTO `tgs_prueba`.`persona_tipo` (`id`, `nombre`) VALUES (1, 'Area');
INSERT INTO `tgs_prueba`.`persona_tipo` (`id`, `nombre`) VALUES (2, 'Persona');

COMMIT;


-- -----------------------------------------------------
-- Data for table `tgs_prueba`.`recurso_estado`
-- -----------------------------------------------------
START TRANSACTION;
USE `tgs_prueba`;
INSERT INTO `tgs_prueba`.`recurso_estado` (`id`, `nombre`) VALUES (1, 'Mal Estado');
INSERT INTO `tgs_prueba`.`recurso_estado` (`id`, `nombre`) VALUES (2, 'Buen Estado');
INSERT INTO `tgs_prueba`.`recurso_estado` (`id`, `nombre`) VALUES (3, 'Nuevo');

COMMIT;


-- -----------------------------------------------------
-- Data for table `tgs_prueba`.`recurso_tipo`
-- -----------------------------------------------------
START TRANSACTION;
USE `tgs_prueba`;
INSERT INTO `tgs_prueba`.`recurso_tipo` (`id`, `nombre`) VALUES (1, 'Tecnológico');
INSERT INTO `tgs_prueba`.`recurso_tipo` (`id`, `nombre`) VALUES (2, 'Oficina');
INSERT INTO `tgs_prueba`.`recurso_tipo` (`id`, `nombre`) VALUES (3, 'Cafetería');

COMMIT;


-- -----------------------------------------------------
-- Data for table `tgs_prueba`.`proveedor`
-- -----------------------------------------------------
START TRANSACTION;
USE `tgs_prueba`;
INSERT INTO `tgs_prueba`.`proveedor` (`id`, `nombre`) VALUES (1, 'TIGOUNE');
INSERT INTO `tgs_prueba`.`proveedor` (`id`, `nombre`) VALUES (2, 'CLARO');
INSERT INTO `tgs_prueba`.`proveedor` (`id`, `nombre`) VALUES (3, 'IMUSA');
INSERT INTO `tgs_prueba`.`proveedor` (`id`, `nombre`) VALUES (4, 'LENOVO');

COMMIT;
