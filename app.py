import os
from flask import (Flask,jsonify,request)
from flask_cors import CORS
from models.models import db
import db as dbLite

def create_app():
    """Create and configure an instance of the Flask application."""
    app = Flask(__name__, instance_relative_config=True)
    CORS(app)
    app.config.from_mapping(
        # a default secret that should be overridden by instance config
        SECRET_KEY='dev',
        # store the database in the instance folder
        DATABASE='./db/tgs.db',
        #configuracion para MySQL
        #SQLALCHEMY_DATABASE_URI = 'mysql+mysqldb://root:1234@localhost:3306/tgs_prueba',
        #configuracion para SQLite
        SQLALCHEMY_DATABASE_URI = 'sqlite:///db/tgs.db',
        SQLALCHEMY_TRACK_MODIFICATIONS = False,
    )
    # ensure the instance folder exists
    @app.route('/doc')
    def hello_world():
        return 'Aca ira la documentacion del proyeto'


    # register the database commands
    db.init_app(app);
    dbLite.init_app(app);

    # apply the blueprints to the app
    import persona, recurso
    app.register_blueprint(persona.bp)
    app.register_blueprint(recurso.bp)
    # make url_for('index') == url_for('blog.index')
    # in another app, you might define a separate main index here with
    # app.route, while giving the blog blueprint a url_prefix, but for
    # the tutorial the blog will be the main index
    #app.add_url_rule('/', endpoint='index')

    return app
