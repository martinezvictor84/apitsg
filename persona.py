from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for,jsonify
)
import json
from werkzeug.exceptions import abort
from models.models import Persona
from models.models import PersonaTipo
from models.models import db
from sqlalchemy.ext.serializer import loads, dumps

bp = Blueprint('persona', __name__)

""" PERSONAS """
""" Obtiene un listado con las personas disponibles"""
@bp.route('/persona', methods=('GET',))
def get_tipos_persona():
    personas = Persona.query.filter_by(enabled = True)
    data =[ persona.serialize() for persona in personas]
    return jsonify(data)

""" Obtiene un listado con los tipos de persona disponibles"""
@bp.route('/persona/tipo', methods=('GET',))
def get_personas():
    personas = PersonaTipo.query.all()
    data =[ persona.serialize() for persona in personas]
    return jsonify(data)

""" Obtiene la informacion de una persona"""
@bp.route('/persona/<int:id>', methods=('GET',))
def get_persona(id):
    persona = Persona.query.get(id)
    if persona:
        return jsonify(persona.serialize())
    return jsonify(cod = 404 , mensaje="recurso no existente"),404

""" Crea un nuevo persona"""
@bp.route('/persona', methods=('POST',))
def create_persona():
    cod = 200
    mensaje = "guarado exitoso"
    id=''
    data = request.form.copy()
    if(request.is_json):
        data = request.json
    try:
        persona = Persona(
            persona_tipo_id =data['persona_tipo_id'],
            nombre = data['nombre'],
            telefono = data['telefono']
        )
        db.session.add(persona)
        db.session.commit()
        id=persona.id
    except Exception as e:
        cod = 400
        mensaje = "Bad Request"
        print(e)
    return jsonify(cod=cod,mensaje=mensaje,id=id),cod

""" Actualiza informacion de un recurso una persona"""
@bp.route('/persona/<int:id>', methods=('PUT',))
def update_persona(id):
    cod = 404
    mensaje = "recurso no encontrado"
    persona = Persona.query.get(id)
    data = request.form.copy()
    if(request.is_json):
        data = request.json
    if(persona):
        try:
            persona.persona_tipo_id =data['persona_tipo_id']
            persona.nombre = data['nombre']
            persona.telefono = data['telefono']
            db.session.commit()
            cod = 200
            mensaje = "Actualizacion de datos completa"
        except Exception as e:
            cod = 400
            mensaje = "Bad request"
    return jsonify(cod = cod , mensaje = mensaje) , cod

""" Borra(inhabilita) una persona"""
@bp.route('/persona/<int:id>', methods=('DELETE',))
def delate_persona(id):
    persona = Persona.query.get(id)
    cod = 404
    mensaje = "recurso no encontrado"
    if(persona):
        try:
            persona.enabled = False
            db.session.commit()
            cod = 200
            mensaje = "Persona Borrada"
        except Exception as e:
            cod = 500
            mensaje = "ha ocurrrido un error inesperado"
    return jsonify(cod = cod , mensaje = mensaje) , cod
