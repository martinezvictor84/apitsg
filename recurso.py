from flask import (
    Blueprint, flash, g, request, url_for,jsonify
)
from werkzeug.exceptions import abort
from datetime import datetime
from sqlalchemy import exc
from models.models import db
from models.models import Proveedor
from models.models import RecursoEstado
from models.models import Recurso
from models.models import Persona
from models.models import RecursoTipo


bp = Blueprint('recurso', __name__)

""" Obtiene un listado con los recursos disponibles"""
""" Tipo, Serial, Marca, responsable"""
@bp.route('/recurso', methods=('GET',))
def get_recursos():
    tipo = request.args.get('tipo')
    estado = request.args.get('estado')
    serial = request.args.get('serial')
    marca = request.args.get('marca')
    responsable = request.args.get('responsable')
    query = Recurso.query
    if tipo:
        query = query.filter(Recurso.recurso_tipo_id == tipo)
    if estado:
        query = query.filter(Recurso.recurso_estado_id == estado)
    if marca:
        query = query.filter(Recurso.marca.like('%'+marca+'%'))
    if responsable:
        query = query.join(Persona).filter(Persona.nombre.like('%'+responsable+'%'))
    if serial:
        query = query.filter(Recurso.serial.like('%'+serial+'%'))

    recursos= query.all()
    # if(tipo):

    data = [recurso.serialize() for recurso in recursos]
    return jsonify(data)

""" Obtiene la informacion de un recurso"""
@bp.route('/recurso/<int:id>', methods=('GET',))
def get_recurso(id):
    recurso =Recurso.query.get(id)
    if(recurso):
        data = recurso.serialize()
        return jsonify(data)
    return jsonify(cod = 404 , mensaje="recurso no existente"),404

""" Crea un nuevo recruso"""
@bp.route('/recurso', methods=('POST',))
def create_recurso():
    cod =200
    mensaje = "guarado exitoso"
    id=""
    data = request.form.copy()
    if(request.is_json):
        data = request.json
    if not ('persona_id' in data) or data['persona_id'] =='' or data['persona_id'] == '0' :
        data['persona_id'] = None
    try:
        recurso = Recurso(
            serial = data['serial'],
            marca = data['marca'],
            nombre = data['nombre'],
            recurso_estado_id  = data['recurso_estado_id'],
            proveedor_id = data['proveedor_id'],
            persona_id = data['persona_id'],
            recurso_tipo_id = data['recurso_tipo_id'],
            asignado_en = datetime.now() if data['persona_id'] else None
        )
        db.session.add(recurso)
        db.session.commit()
        id=recurso.id
    except exc.IntegrityError as e:
        cod = 400
        mensaje = "Serial ya esta siendo usado"
        print(e)
    except Exception as e:
        cod = 400
        mensaje = "Bad request"
    return jsonify(cod=cod , mensaje = mensaje,id=id) , cod

""" Actualiza informacion de un recurso recruso"""
@bp.route('/recurso/<int:id>', methods=('PUT',))
def update_recurso(id):
    cod =200
    mensaje = "guarado exitoso"
    recurso =Recurso.query.get(id)
    if(recurso):
        data = request.form.copy()
        if(request.is_json):
            data = request.json
        if not ('persona_id' in data) or data['persona_id'] =='' or data['persona_id'] == '0' :
            data['persona_id'] = None
        try:
            recurso.serial = data['serial']
            recurso.marca = data['marca']
            recurso.nombre = data['nombre']
            recurso.recurso_estado_id  = data['recurso_estado_id']
            recurso.proveedor_id =data['proveedor_id']
            recurso.persona_id = data['persona_id']
            recurso.recurso_tipo_id = data['recurso_tipo_id']
            recurso.asignado_en = datetime.now() if data['persona_id'] else None
            db.session.commit()
        except exc.IntegrityError as e:
            cod = 400
            mensaje = "Serial ya esta siendo usado"
            print(e)
        except Exception as e:
            cod = 400
            mensaje = "Bad request"
    else:
        cod = 404
        mesanje = "recurso no existente"

    return jsonify(cod=cod , mensaje = mensaje) , cod

""" Borra(inhabilita) un recurso"""
@bp.route('/recurso/<int:id>', methods=('DELETE',))
def delate_recursos(id):
    cod =404
    mensaje = "recurso no exiatente"
    recurso =Recurso.query.get(id)
    if(recurso):
        db.session.delete(recurso)
        db.session.commit()
        cod = 200
        mensaje = "borrado satisfactoriamente"
    return jsonify(cod=cod , mensaje = mensaje) , cod

""" Obtiene un listado con los proveedores disponibles"""
@bp.route('/recurso/proveedor', methods=('GET',))
def get_proveedores():
   proveedores =Proveedor.query.all()
   data = [proveedor.serialize() for proveedor in proveedores]
   return jsonify(data)

""" Obtiene un listado con los estados disponibles"""
@bp.route('/recurso/estado', methods=('GET',))
def get_estados():
   estados =RecursoEstado.query.all()
   data = [estado.serialize() for estado in estados]
   return jsonify(data)

""" Obtiene un listado con los tipos disponibles"""
@bp.route('/recurso/tipo', methods=('GET',))
def get_tipos():
   tipos = RecursoTipo.query.all()
   data = [tipo.serialize() for tipo in tipos]
   return jsonify(data)
